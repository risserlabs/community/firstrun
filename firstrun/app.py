# File: /firstrun/app.py
# Project: firstrun
# File Created: 05-06-2022 04:12:47
# Author: Clay Risser
# -----
# Last Modified: 09-06-2022 10:26:13
# Modified By: Clay Risser
# -----
# Risser Labs LLC (c) Copyright 2022
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import asyncio
from gtklara import GTKlara, Gtk
from views import Home
from fragments import MultilineEntry, Box, Entry, Bool, ComboBox


class Label(GTKlara):
    def __init__(self, children=[], **kwargs):
        super().__init__(children, kwargs, Gtk.Label())


class App(GTKlara):
    def __init__(self, children=[], **kwargs):
        self._input = None
        super().__init__(children, kwargs)
        self._questions = self._prop("questions")
        self._questions.state.bind(self.handle_question_change)
        self._question = None
        self._answers = {}

    def handle_question_change(self, value):
        title = value["title"]
        _type = value["type"]
        description = value["description"] if "description" in value else title
        question = None
        Question = Entry
        if _type == "multiline":
            Question = MultilineEntry
        elif _type == "bool":
            Question = Bool
        elif _type == "enum":
            question = ComboBox(
                title=title,
                description=description,
                on_changed=self.handle_answer,
                items={"one": "ONE", "two": "TWO"},
            )
        if not question:
            question = Question(
                title=title,
                description=description,
                on_changed=self.handle_answer,
            )
        self._input.clear()
        self._input.add(question)
        if "key" in value:
            question.set_value(value["key"])
        self._question = question

    def handle_answer(self, answer, key=None):
        if type(answer) is str:
            answer = answer.strip()
        if answer is not None and (type(answer) is not str or len(answer) > 0):
            self._questions.answer(answer, key)
        self.set_next_button(answer)

    def set_next_button(self, answer):
        if type(answer) is str:
            answer = answer.strip()
        if answer is not None and (type(answer) is not str or len(answer) > 0):
            self._home.disable_next(False)
        else:
            self._home.disable_next()

    def render(self):
        self._input = Box()
        self._home = Home(
            self._input,
            on_next_clicked=self.handle_next_clicked,
            on_previous_clicked=self.handle_previous_clicked,
        )
        return self._home

    async def handle_next_clicked(self, widget):
        self._questions.next()
        self.set_next_button(self._questions.current_key)
        self._home.disable_previous(False)
        if self._questions.end:
            if self._home.next_label == "Done":
                self._input.clear()
                self._home.start_spinner()
                await self.process()
            self._home.next_is_done()

    def handle_previous_clicked(self, widget):
        self._questions.previous()
        self._home.next_is_done(False)
        if self._questions.begin:
            self._home.disable_previous()
        if self._questions.current_key is None:
            self._home.disable_next(True)
        else:
            self._home.disable_next(False)

    async def process(self):
        self._home.disable_next()
        self._home.disable_previous()
        await asyncio.sleep(3)
        GTKlara.main_quit()
