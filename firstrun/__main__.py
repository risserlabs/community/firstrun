# File: /firstrun/__main__.py
# Project: firstrun
# File Created: 06-06-2022 11:25:32
# Author: Clay Risser
# -----
# Last Modified: 09-06-2022 12:25:42
# Modified By: Clay Risser
# -----
# Risser Labs LLC (c) Copyright 2022
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from window import Window
from app import App
from questions import Questions
from gtklara import GTKlara
from gi.repository import Gdk


def handle_window_state_event(window, event):
    if not (Gdk.WindowState.FULLSCREEN & int(event.new_window_state)):
        window.fullscreen()


def main():
    questions = Questions()
    window = Window(App(questions=questions))
    window.connect("destroy", lambda *args: main())
    window.connect("window-state-event", handle_window_state_event)
    window.fullscreen()
    window.show_all()


if __name__ == "__main__":
    main()
    GTKlara.main()
