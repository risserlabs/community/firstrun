# File: /firstrun/questions.py
# Project: firstrun
# File Created: 07-06-2022 10:18:48
# Author: Clay Risser
# -----
# Last Modified: 08-06-2022 15:29:26
# Modified By: Clay Risser
# -----
# Risser Labs LLC (c) Copyright 2022
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from gtklara import State


class Questions:
    def __init__(
        self,
        questions=[
            {"title": "zero", "description": "i am zero", "type": "enum"},
            {"title": "one", "description": "i am one", "type": "multiline"},
            {"title": "two", "description": "i am three", "type": "bool"},
            {"title": "three", "description": "i am three", "type": "text"},
            {"title": "four", "description": "i am four", "type": "enum"},
        ],
    ):
        self._state = State(questions[0])
        self._questions = questions
        self._current_index = 0

    @property
    def begin(self):
        return self._current_index <= 0

    @property
    def end(self):
        return self._current_index >= len(self._questions) - 1

    def next(self):
        if len(self._questions) <= 0:
            return None
        self._current_index = min(self._current_index + 1, len(self._questions) - 1)
        self.state.set(self._questions[self._current_index])
        return self._questions[self._current_index]

    def previous(self):
        if len(self._questions) <= 0:
            return None
        self._current_index = max(self._current_index - 1, 0)
        self.state.set(self._questions[self._current_index])
        return self._questions[self._current_index]

    def answer(self, answer, key=None):
        self._questions[self._current_index]["answer"] = answer
        self._questions[self._current_index]["key"] = answer if key is None else key

    @property
    def state(self):
        return self._state

    @property
    def questions(self):
        return self._questions

    @property
    def current_answer(self):
        current = self._questions[self._current_index]
        if "answer" in current:
            return current["answer"]
        return None

    @property
    def current_key(self):
        current = self._questions[self._current_index]
        if "key" in current:
            return current["key"]
        return self.current_answer
