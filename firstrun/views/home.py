# File: /firstrun/views/home.py
# Project: firstrun
# File Created: 05-06-2022 09:42:59
# Author: Clay Risser
# -----
# Last Modified: 08-06-2022 13:28:13
# Modified By: Clay Risser
# -----
# Risser Labs LLC (c) Copyright 2022
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from gtklara import GTKlara


class Home(GTKlara):
    signal_bindings = {
        "next_clicked": "next:clicked",
        "previous_clicked": "previous:clicked",
    }

    def __init__(self, children=[], **kwargs):
        super().__init__(children, kwargs, __file__)
        self._spinner = self._get("spinner")
        self.stop_spinner()

    def disable_previous(self, value=True):
        self._get("previous").set_sensitive(not value)

    def disable_next(self, value=True):
        self._get("next").set_sensitive(not value)

    def next_is_done(self, value=True):
        self._get("next").set_label("Done" if value else "Next")

    def start_spinner(self):
        self._spinner.start()

    def stop_spinner(self):
        self._spinner.stop()

    @property
    def next_label(self):
        return self._get("next").get_label()
