# File: /firstrun/fragments/box.py
# Project: firstrun
# File Created: 08-06-2022 12:17:55
# Author: Clay Risser
# -----
# Last Modified: 08-06-2022 12:33:30
# Modified By: Clay Risser
# -----
# Risser Labs LLC (c) Copyright 2022
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from gtklara import GTKlara


class Box(GTKlara):
    def __init__(self, children=[], **kwargs):
        super().__init__(children, kwargs, __file__)
