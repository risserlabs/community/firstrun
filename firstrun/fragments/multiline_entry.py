# File: /firstrun/fragments/multiline_entry.py
# Project: firstrun
# File Created: 05-06-2022 09:45:44
# Author: Clay Risser
# -----
# Last Modified: 08-06-2022 15:09:56
# Modified By: Clay Risser
# -----
# Risser Labs LLC (c) Copyright 2022
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from gtklara import GTKlara


class MultilineEntry(GTKlara):
    prop_bindings = {"title": "title:label", "description": "description:label"}

    def __init__(self, children=[], **kwargs):
        super().__init__(children, kwargs, __file__)
        widget = self._active
        widget.get_buffer().connect("changed", self.handle_changed)
        if "on_changed" in self._props:
            self._on_changed = self._prop("on_changed")

    def handle_changed(self, buffer):
        if hasattr(self, "_on_changed"):
            self._on_changed(self.value)

    def set_value(self, answer):
        self._active.get_buffer().set_text(answer, len(answer))

    @property
    def value(self):
        buffer = self._active.get_buffer()
        return buffer.get_text(buffer.get_start_iter(), buffer.get_end_iter(), False)
