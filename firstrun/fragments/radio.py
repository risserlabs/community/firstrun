# File: /firstrun/fragments/radio.py
# Project: firstrun
# File Created: 03-06-2022 11:26:54
# Author: Clay Risser
# -----
# Last Modified: 08-06-2022 15:16:41
# Modified By: Clay Risser
# -----
# Risser Labs LLC (c) Copyright 2022
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from gtklara import GTKlara, Gtk


class Radio(GTKlara):
    prop_bindings = {"title": "title:label", "description": "description:label"}

    def __init__(self, children=[], **kwargs):
        super().__init__(children, kwargs, __file__)
        self._options = self._props["options"] if "options" in self._props else {}
        hidden_radio_button = Gtk.RadioButton("", "")
        hidden_radio_button.set_active(True)
        radio_buttons = []
        previous = hidden_radio_button
        self._value = None
        self._radio_buttons = {}
        for name, description in self._options.items():
            radio_button = Gtk.RadioButton(name, description, group=previous)
            self._radio_buttons[name] = radio_button
            previous = radio_button
            radio_button.set_active(False)
            radio_button.connect("toggled", self.handle_toggled)
            radio_buttons.append(radio_button)
        self._clear(self._active)
        self._add(self._active, radio_buttons)
        self._active.show_all()
        if "on_changed" in self._props:
            self._on_changed = self._props["on_changed"]

    def handle_toggled(self, widget):
        key = widget.get_label()
        self._value = self._options[key] if key in self._options else None
        if widget.get_active():
            if hasattr(self, "_on_changed"):
                self._on_changed(self.value, key)

    def set_value(self, key):
        for name, radio_button in self._radio_buttons.items():
            if name == key:
                radio_button.set_active(True)
            else:
                radio_button.set_active(False)

    @property
    def value(self):
        return self._value
