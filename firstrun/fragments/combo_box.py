# File: /firstrun/fragments/combo_box.py
# Project: firstrun
# File Created: 05-06-2022 09:45:44
# Author: Clay Risser
# -----
# Last Modified: 08-06-2022 15:07:53
# Modified By: Clay Risser
# -----
# Risser Labs LLC (c) Copyright 2022
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from gtklara import GTKlara


class ComboBox(GTKlara):
    ignore_signals = ["changed"]
    prop_bindings = {"title": "title:label", "description": "description:label"}

    def __init__(self, children=[], **kwargs):
        super().__init__(children, kwargs, __file__)
        self._items = self._props["items"] if "items" in self._props else {}
        widget = self._active
        for key in self._items.keys():
            widget.append_text(key)
        widget.connect("changed", self.handle_changed)
        if "on_changed" in self._props:
            self._on_changed = self._prop("on_changed")

    def handle_changed(self, widget):
        key = self._active.get_active()
        if hasattr(self, "_on_changed"):
            self._on_changed(self.value, key)

    def set_value(self, key):
        self._active.set_active(key)

    @property
    def value(self):
        key = self._active.get_active()
        text = list(self._items.keys())[key]
        return self._items[text] if text in self._items else None
